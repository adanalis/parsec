extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> s d c
 *
 */
#define PRECISION_z

#include "parsec.h"
#include <math.h>
#include <core_blas.h>
#include <core_blas.h>

#include "data_distribution.h"
#include "data_dist/matrix/precision.h"
#include "data_dist/matrix/matrix.h"
#include "dplasma/lib/memory_pool.h"
#include "dplasma/lib/dplasmajdf.h"
#include "dplasma/cores/dplasma_zcores.h"

PLASMA_desc plasma_desc_init(PLASMA_enum dtyp, int mb, int nb, int bsiz,
                             int lm, int ln, int i, int j, int m, int n);

%}

descA   [type = "tiled_matrix_desc_t"]
A       [type = "parsec_ddesc_t *"]
descB   [type = "tiled_matrix_desc_t"]
B       [type = "parsec_ddesc_t *"]
IPIV    [type = "parsec_ddesc_t *" aligned=A]
descLT  [type = "tiled_matrix_desc_t"]
LT      [type = "parsec_ddesc_t *" aligned=A]
descLT2 [type = "tiled_matrix_desc_t"]
LT2     [type = "parsec_ddesc_t *" aligned=A]
pivfct  [type = "qr_piv_t*"]
ib      [type = int]
p_work  [type = "parsec_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)*ib*(descLT.nb))"]
p_tau   [type = "parsec_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)   *(descLT.nb))"]
INFO    [type = "int*"]

param_p [type = int default="pivfct->p"      hidden=on]
param_a [type = int default="pivfct->a"      hidden=on]
param_d [type = int default="pivfct->domino" hidden=on]


/********************************************************************************************
 *
 *                               SWAP + TRSM
 *
 ********************************************************************************************/
swptrsm(k, i, n)
  /* Execution space */
  k = 0..( (descA.mt < descA.nt) ? descA.mt-1 : descA.nt-1 )
  i = 0..%{ return dplasma_qr_getnbgeqrf( pivfct, k, descA.mt ) - 1; %}
  n = 0..descB.nt-1

  m     = %{ return dplasma_qr_getm( pivfct, k, i ); %}
  nextm = %{ return dplasma_qr_nexttriangle( pivfct, m, k, descA.mt ); %}
  s     = %{ return dplasma_qr_getsize( pivfct, k, i ); %}
  ms    = %{ return m + (s-1)*param_p; %}

  /* Locality */
  :B(m, n)

  READ  A    <- A swptrsm_in(k,i)                                                    [type = LOWER_TILE]
  READ  IP   <- P swptrsm_in(k,i)                                                    [type = PIVOT]
  RW    C    <- ( k == 0 ) ? B(m,n) : C zttmqr(k-1, m, n)
             -> ( k == descA.mt-1 ) ? B(m,n)
             -> ( (k < descA.mt-1 ) & (nextm != descA.mt) ) ?  V zttmqr(k, nextm, n)
             -> ( (k < descA.mt-1 ) & (nextm == descA.mt) ) ?  C zttmqr(k, m, n)
             -> ( (k < descA.mt-1 ) & (m < ms))? V zttmqr(k, (m+param_p)..ms..param_p, n)
  CTL   ctl  <- ( k > 0 ) ? ctl tile2panel(k-1, m, n )
  RW   T  <- T swptrsm_in(k,i)                                                           [type = LITTLE_T]

/* Priority */
;descB.nt-n-1

BODY
{
    int tempAnn = (n == (descA.nt-1)) ? (descA.n - n * descA.nb) : descA.nb;
    int tempBnn = (n == (descB.nt-1)) ? (descB.n - n * descB.nb) : descB.nb;
    int tempmm  = (m == (descB.mt-1)) ? (descB.m - m * descB.mb) : descB.mb;
    int tempm   = (s-1) * descA.mb +
        (( ms == descA.mt-1 ) ? descA.m - ms * descA.mb : descA.mb);
    int tempmin = dplasma_imin( tempmm, tempAnn );
    int ldam = BLKLDD(descA, m);
    int ldbm = BLKLDD(descB, m);

    printlog("\nthread %d VP %d zswptrsm_hpp( k=%d, i=%d, n=%d, m=%d, nextm=%d, s=%d, ms=%d )\n"
             "\t( MA=%d, NA=%d, MB=%d, NB=%d, A(%d,%d)[%p], lda=%d, C(%d, %d)[%p], ldc=%d, IPIV(%d,%d)[%p])\n",
             context->th_id, context->virtual_process->vp_id, k, i, n, m, nextm, s, ms,
             tempmm, tempAnn, tempm, tempBnn, m, k, A, ldam, m, n, C, ldam, m, k, IP);

#if !defined(PARSEC_DRY_RUN)


        if (s!=1) 
        {
        PLASMA_desc pdescB = plasma_desc_init( PlasmaComplexDouble,
                                               descB.mb, descB.nb, descB.mb * descB.nb,
                                               s*descB.mb, descB.nb, 0, 0,
                                               tempm, tempBnn);
        pdescB.mat = (void*)C;

        CORE_zlaswp_ontile(pdescB, 1, tempmin, IP, 1);

        CORE_ztrsm(
            PlasmaLeft, PlasmaLower, PlasmaNoTrans, PlasmaUnit,
            tempmm, tempBnn,
             1., A /*A(m, k)*/, ldam,
                 C /*B(m, n)*/, ldbm);
        }
        else
        {
         void *p_elem_A = parsec_private_memory_pop( p_work );

         int tempmm = (m == (descB.mt-1)) ? (descB.m - m * descB.mb) : descB.mb;
         int tempnn = (n == (descB.nt-1)) ? (descB.n - n * descB.nb) : descB.nb;
         int ldbm   = BLKLDD( descB, m );

         CORE_zunmqr(
                     PlasmaLeft, PlasmaConjTrans,
                     tempmm, tempnn, tempmm, ib,
                     A /* A(m, k) */, ldbm,
                     T /* T(m, k) */, descLT.mb,
                     C /* A(m, n) */, ldbm,
                     p_elem_A, descLT.nb );

         parsec_private_memory_push( p_work, p_elem_A );

        }
#endif /* !defined(PARSEC_DRY_RUN) */
}
END

swptrsm_in(k, i)
  k = 0..( (descA.mt < descA.nt) ? descA.mt-1 : descA.nt-1 )
  i = 0..%{ return dplasma_qr_getnbgeqrf( pivfct, k, descA.mt ) - 1; %}
  m = %{ return dplasma_qr_getm( pivfct, k, i); %}

  : A(m,k)

  READ A <- A(m,k)                             [type = LOWER_TILE]
         -> A swptrsm(k, i, 0..descB.nt-1)     [type = LOWER_TILE]
  READ P <- IPIV(m,k)                          [type = PIVOT]
         -> IP swptrsm(k, i, 0..descB.nt-1)    [type = PIVOT]
  READ T <- LT2(m,k)                            [type = LITTLE_T]
         -> T swptrsm(k, i, 0..descB.nt-1)     [type = LITTLE_T]

BODY
    /*Nothing*/
END

/********************************************************************************************
 *
 *                                 TTMQR kernel
 *
 * See TTQRT
 *
 * type1 defines the operations to perfom at next step k+1 on the row m
 *   if type1 == 0, it will be a TS so the tile goes to a TTQRT/TTMQR operation
 *   if type1 != 0, it will be a TT so the tile goes to a GEQRT/UNMQR operation
 * im1 is the index of the killer m at the next step k+1 if its type is !0, descA.mt otherwise
 *
 ********************************************************************************************/

zttmqr(k, m, n)
  /* Execution space */
  k = 0   .. ( (descA.mt < descA.nt) ? descA.mt-1 : descA.nt-1 )
  m = k+1 .. descA.mt-1
  n = 0   .. descB.nt-1

  p        = %{ return dplasma_qr_currpiv(pivfct, m, k); %}
  nextp    = %{ return dplasma_qr_nexttriangle(pivfct, p, k, m); %}
  prevp    = %{ return dplasma_qr_prevtriangle(pivfct, p, k, m); %}
  prevm    = %{ return dplasma_qr_prevtriangle(pivfct, m, k, m); %}
  type     = %{ return dplasma_qr_gettype( pivfct, k,   m ); %}
  nexttype = %{ return dplasma_qr_gettype( pivfct, k+1, m ); %}
  ip       = %{ return dplasma_qr_geti(    pivfct, k,   p ); %}
  im       = %{ return (type     == 0) ? -1 : dplasma_qr_geti( pivfct, k,   m ); %}
  nextim   = %{ return (nexttype == 0) ? -1 : dplasma_qr_geti( pivfct, k+1, m ); %}

  : B(m, n)

  RW   V  <- ( prevp == descA.mt ) ? C swptrsm( k, ip, n )
          <- ( prevp != descA.mt ) ? V zttmqr(k, prevp, n )

          -> ( (type != 0) & (nextp != descA.mt)              ) ? V zttmqr( k, nextp, n)
          -> ( (type != 0) & (nextp == descA.mt) & ( p == k ) ) ? A zttmqr_out(k, n)
          -> ( (type != 0) & (nextp == descA.mt) & ( p != k ) ) ? C zttmqr( k, p, n )

  RW   C  <- ( (type == 0) && (k == 0) ) ? B(m, n)
          <- ( (type == 0) && (k != 0) ) ? C zttmqr(k-1, m, n )
          <- ( (type != 0) && (prevm == descA.mt) ) ? C swptrsm(k, im, n)
          <- ( (type != 0) && (prevm != descA.mt) ) ? V zttmqr(k, prevm, n )

          -> (nexttype != 0 ) ? C swptrsm( k+1, nextim, n )
          -> (nexttype == 0 ) ? C zttmqr( k+1, m, n )

  READ  H <- A zttmqr_in(k,m)
  READ  T <- ( type == 0 ) ? B(m, n)             /*UNUSED*/
          <- ( type != 0 ) ? T zttmqr_in(k,m)    [type = LITTLE_T]

  CTL ctlp <- ( (type != 0) && (prevp == descA.mt) ) ? ctl low2high(k, p, n)
  CTL ctlm <- ( (type != 0) && (prevm == descA.mt) ) ? ctl low2high(k, m, n)
  CTL ctl  -> ( type == 0 ) ? ctl tile2panel(k, m, n)
           -> ( type == 0 ) ? ctl low2high(k, m, n)

  ;descB.nt-n-1

BODY
{
    int tempmm  = (m == (descA.mt-1)) ? descA.m - m * descA.mb : descA.mb;
    int tempAkn = (k == (descA.nt-1)) ? descA.n - k * descA.nb : descA.nb;
    int tempBnn = (n == (descB.nt-1)) ? descB.n - n * descB.nb : descB.nb;
    int ldam = BLKLDD( descA, m );
    int ldbp = BLKLDD( descB, p );
    int ldbm = BLKLDD( descB, p );
    int ldwork = ib;

    printlog("\nthread %d VP %d zttmqr( k=%d, m=%d, n=%d, p=%d, nextp=%d, prevp=%d, prevm=%d, type=%d, nexttype=%d, ip=%d, im=%d, nextim=%d)\n"
             "\t(M1=%d, N1=%d, M2=%d, N2=%d, ib=%d, \n"
             "\t A1(%d,%d)[%p], lda1=%d, A2(%d,%d)[%p], lda2 = %d,\n"
             "\t LT(%d,%d)[%p], descLT.mb=%d, A(%d,%d)[%p], lda = %d)\n",
             context->th_id, context->virtual_process->vp_id, k, m, n, p, nextp, prevp, prevm, type, nexttype, ip, im, nextim,
             descA.mb, tempBnn, tempmm, tempBnn, ib,
             p, n, V, ldbp,      m, n, C, ldam,
             m, k, T, descLT.mb, m, k, H, ldam);

#if !defined(PARSEC_DRY_RUN)
      if ( type == DPLASMA_QR_KILLED_BY_TS ) {

          /* TODO: Check the K in the gemm for limit cases */
          CORE_zgemm(PlasmaNoTrans, PlasmaNoTrans,
                     tempmm, tempBnn, tempAkn, /*descB.mb,*/
                     -1., H /*A(m, k)*/, ldam,
                          V /*B(p, n)*/, ldbp,
                     1.,  C /*B(m, n)*/, ldbm );

      } else {
          void *p_elem_A = parsec_private_memory_pop( p_work );

          CORE_zttmqr(
              PlasmaLeft, PlasmaConjTrans,
              descB.mb, tempBnn, tempmm, tempBnn, tempAkn, ib,
              V /* B(p, n) */, ldbp,
              C /* B(m, n) */, ldbm,
              H /* A(m, k) */, ldam,
              T /* T(m, k) */, descLT.mb,
              p_elem_A, ldwork );

          parsec_private_memory_push( p_work, p_elem_A );
      }
#endif /* !defined(PARSEC_DRY_RUN) */
}
END


zttmqr_out(k, n) [profile = off]
  k = 0 .. ( (descA.mt < descA.nt) ? descA.mt-2 : descA.nt-2 )
  n = 0 .. descB.nt-1
  prevp = %{ return dplasma_qr_prevpiv(pivfct, k, k, k); %}
  type  = %{ return dplasma_qr_gettype(pivfct, k, prevp); %}

  : B(k, n)

  RW A <- (type == 0) ? B(k, n) : V zttmqr( k, prevp, n )
       -> B(k, n)
BODY
    printlog("\nthread %d VP %d ttmqr_out( k = %d, n = %d ) type = %d\n",
             context->th_id, context->virtual_process->vp_id, k, n, type);
END


zttmqr_in(k, m)
  k = 0..( (descA.mt < descA.nt) ? descA.mt-1 : descA.nt-1 )
  m = k+1..descA.mt-1
  type = %{ return dplasma_qr_gettype(pivfct, k, m); %}

  : A(m,k)

  READ A <- A(m,k)
         -> H zttmqr(k, m, 0..descB.nt-1)
  READ T <- LT(m,k)
         -> (type != 0) ? T zttmqr(k, m, 0..descB.nt-1)

BODY
    /*Nothing*/
END

/************************************************************************************
 *                      Low level to low level (Forward)                            *
 *            step k on tile(m, n) is done when low2high(k, m, n) is released       *
 ************************************************************************************/
low2high(k, m, n) [ profile = off ]
  k = 0 .. ( (descA.mt < descA.nt) ? descA.mt-1 : descA.nt-1 )
  m = k .. descA.mt-1
  n = 0 .. descB.nt-1

  type = %{ return dplasma_qr_gettype( pivfct, k, m ); %}
  p    = %{ return type == 0 ? dplasma_qr_currpiv(pivfct, m, k ) : m; %}
  i    = %{ return dplasma_qr_geti( pivfct, k, p ); %}
  s    = %{ return dplasma_qr_getsize( pivfct, k, i ); %}
  ms   = %{ return p + (s-1)*param_p; %}
  kp   = %{ return dplasma_qr_nexttriangle(pivfct, p, k, descA.mt); %}

  :B(m, n)

  CTL  ctli <- (m < ms) ? ctli low2high(k, m+param_p, n)
            -> (m > p ) ? ctli low2high(k, m-param_p, n)

  CTL  ctl  <- (type == 0) ? ctl zttmqr(k, m, n)
            /* Protect step k */
            -> ( (m == p) & (kp != descA.mt)            ) ? ctlp zttmqr(k, kp, n)
            -> ( (m == p) & (kp == descA.mt) & (p != k) ) ? ctlm zttmqr(k, m,  n)
            /* Protect step k+1 ( See if it's possible to merge tile2panel in low2high */
            /* -> ( (m == p) & ((nexttype+1) != 0) & (n == k+1) ) ? ctl zgetrf_hpp(k+1, nexti) */
            /* -> ( (m == p) & ((nexttype+1) != 0) & (n >  k+1) ) ? ctl swptrsm(k+1, nexti, n) */

  ;descB.nt-n-1

BODY
    printlog("\nthread %d VP %d low2high( k = %d, m = %d, n = %d, kp = %d ) type = %d\n",
             context->th_id, context->virtual_process->vp_id, k, m, n, kp, type);
END

/************************************************************************************
 *                      Tile 2 panel (Forward)                                      *
 *         Insure that step k on panel n is done before to start step k+1           *
 ************************************************************************************/
tile2panel(k, m, n) [ profile = off ]
  k = 0   .. ( (descA.mt < descA.nt) ? descA.mt-1 : descA.nt-1 )
  m = k+1 .. descA.mt-1
  n = 0   .. descB.nt-1

  type     = %{ return dplasma_qr_gettype( pivfct, k,   m ); %}
  nexttype = %{ return dplasma_qr_gettype( pivfct, k+1, m ); %}
  p        = %{ return nexttype == 0 ? dplasma_qr_currpiv(pivfct, m, k+1 ) : m; %}
  i        = %{ return dplasma_qr_geti( pivfct, k+1, p ); %}
  s        = %{ return dplasma_qr_getsize( pivfct, k+1, i ); %}
  ms       = %{ return p + (s-1)*param_p; %}

  :B(m, n)

  CTL  ctli <- (m < ms) ? ctli tile2panel(k, m+param_p, n)
            -> (m > p ) ? ctli tile2panel(k, m-param_p, n)

  CTL  ctl  <- (type == 0) ? ctl zttmqr(k, m, n)
            /* Protect step k+1 */
            -> (m == p) ? ctl swptrsm(k+1, i, n)

  ;descB.nt-n-1

BODY
    printlog("\nthread %d VP %d tile2panel( k = %d, m = %d, n = %d ) type = %d\n",
             context->th_id, context->virtual_process->vp_id, k, m, n, type);
END
