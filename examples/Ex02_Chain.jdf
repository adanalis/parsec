extern "C" %{

/**
 * This example shows how to create a chain that creates a data and makes it
 * circulate from one task to another
 *    private variables
 *    guarded calls, RW
 *    parsec_arena_construct()
 *
 */

#include <parsec.h>
#include <parsec/data_distribution.h>
#include <parsec/datatype.h>

%}

/**
 * The JDF file can contains globals that will be variables global to all tasks.
 * These are not global variables to the whole code, but only to the scope of
 * the instantiated handle, so multiple instances of the same algorithm will
 * have different values.
 */
NB   [ type="int" ]

Task(k)

k = 0 .. NB

: taskdist( k )

/**
 * Let's ask the engine to create a data that will circulate from the first task
 * to the last one.
 */
RW  A <- (k == 0) ? NEW : A Task( k-1 )
      -> (k < NB) ? A Task( k+1 )

BODY
{
    int *Aint = (int*)A;

    if ( k == 0 ) {
        *Aint = 0;
    } else {
        *Aint += 1;
    }

    printf("I am element %d in the chain\n", *Aint );
}
END

extern "C" %{

static uint32_t
rank_of(parsec_ddesc_t *desc, ...)
{
    (void)desc;
    return 0;
}

static int32_t
vpid_of(parsec_ddesc_t *desc, ...)
{
    (void)desc;
    return 0;
}

static uint64_t
data_key(parsec_ddesc_t *desc, ...)
{
    int k;
    va_list ap;
    (void)desc;
    va_start(ap, desc);
    k = va_arg(ap, int);
    va_end(ap);
    return (uint64_t)k;
}

int main(int argc, char *argv[])
{
    parsec_context_t* parsec;
    int rank, world;
    parsec_ddesc_t taskdist;
    parsec_Ex02_Chain_handle_t *handle;

#if defined(PARSEC_HAVE_MPI)
    {
        int provided;
        MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
    }
    MPI_Comm_size(MPI_COMM_WORLD, &world);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#else
    world = 1;
    rank = 0;
#endif

    parsec = parsec_init(-1, &argc, &argv);

    parsec_ddesc_init(&taskdist, world, rank);
    taskdist.rank_of = rank_of;
    taskdist.vpid_of = vpid_of;
    taskdist.data_key = data_key;

    handle = parsec_Ex02_Chain_new(&taskdist, 10);

    /**
     * Since we create a data on the fly, we need to tell the runtime the
     * datatype of the DEFAULT type, so it can allocate the required memory
     * space.
     */
    parsec_arena_construct(handle->arenas[PARSEC_Ex02_Chain_DEFAULT_ARENA],
                          sizeof(int), PARSEC_ARENA_ALIGNMENT_SSE,
                          parsec_datatype_int_t );

    parsec_enqueue( parsec, (parsec_handle_t*)handle );
    parsec_context_wait(parsec);

    parsec_handle_free((parsec_handle_t*)handle);
    parsec_ddesc_destroy(&taskdist);

    parsec_fini(&parsec);
#if defined(PARSEC_HAVE_MPI)
    MPI_Finalize();
#endif

    return 0;
}

%}
