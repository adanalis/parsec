/*
 * Copyright (c) 2012-2015 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 */


#ifndef PARSEC_TYPES_H_HAS_BEEN_INCLUDED
#define PARSEC_TYPES_H_HAS_BEEN_INCLUDED

typedef struct parsec_data_s parsec_data_t;
typedef struct parsec_data_copy_s parsec_data_copy_t;
typedef uint32_t parsec_data_key_t;
typedef struct parsec_ddesc_s parsec_ddesc_t;

#endif  /* PARSEC_TYPES_H_HAS_BEEN_INCLUDED */

